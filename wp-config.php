<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'puzib_saen');

/** Имя пользователя MySQL */
define('DB_USER', 'puzib_saen');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'p3ux2uzt');

/** Имя сервера MySQL */
define('DB_HOST', 'puzib.mysql.ukraine.com.ua');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TI9.glVe6AwhKC,ql:8j?o[dre_4;Y-KC5BYQ`X+m3[C0Tt2A5{V-(N!tq5q bqr');
define('SECURE_AUTH_KEY',  'ydU>hM;{HZphw%$6H}+>[ZG.CdDKubEx{y,b=:>;^UQH!SM@8HVMS5DxUUdi)WQ@');
define('LOGGED_IN_KEY',    '^Y|B=.T2Nm:T]w=ioYT:,/1DSEq7!*56E`XSMaEoltkTw^kbcc!d[$#CHuI?+rM9');
define('NONCE_KEY',        'ig]VhZvWcJ[:rt%QUBLF -VsES5|D=bEtip1!MBV K<mFgvx(?@jn63LJ *;Fm#W');
define('AUTH_SALT',        '6[Gj`q%Wq;5X~4z?{i8,4bd>Ga@nm$m]RntBb%.2FfiG(4v]}jzI@}xR`.K>Mt0D');
define('SECURE_AUTH_SALT', 'V|V~JN(J[<G#B[&DdNCi=$+Ps3zw%M:gecq3V)YljLe{Ij)1I%XuM1MLf(yXY6Ah');
define('LOGGED_IN_SALT',   'KQ$=p=cn#>RB3VP/+-kZNY#+D6#y@#q/FT2_&Y7zam`JuwCyWVem<XU^s9PK3LSW');
define('NONCE_SALT',       ':2v:x0<]i/mB62erHjO{@`CQ~@Yh?cWn,=NM>(B&0N6RXs{ReGqdtxr*;!I0nDA(');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
