<?php

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}

add_theme_support( 'post-thumbnails' ); // для всех типов постов

function true_apply_categories_for_pages(){
	add_meta_box( 'categorydiv', 'Категории', 'post_categories_meta_box', 'page', 'side', 'normal'); // добавляем метабокс категорий для страниц
	register_taxonomy_for_object_type('category', 'page'); // регистрируем рубрики для страниц
}
// обязательно вешаем на admin_init
add_action('admin_init','true_apply_categories_for_pages');
 
function true_expanded_request_category($q) {
	if (isset($q['category_name'])) // если в запросе присутствует параметр рубрики
		$q['post_type'] = array('post', 'page'); // то, помимо записей, выводим также и страницы
	return $q;
}
 
add_filter('request', 'true_expanded_request_category');

//Добавление "Цитаты" для страниц start
function page_excerpt() {
    add_post_type_support('page', array('excerpt'));
}
add_action('init', 'page_excerpt');
//Добавление "Цитаты" для страниц end


function my_scripts_method() {
	wp_enqueue_script( 'jquery' );
}    
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

show_admin_bar(false);

 
//remove_filter('the_content', 'wpautop');
//remove_filter('the_content', 'wptexturize');

/*
function true_register_wp_sidebars() { 
	
	register_sidebar(
		array(
			'id' => 'top_panel', // уникальный id
			'name' => 'Top panel', // название сайдбара
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
			'before_widget' => '', // по умолчанию виджеты выводятся <li>-списком
			'after_widget' => '',
			'before_title' => '', // по умолчанию заголовки виджетов в <h2>
			'after_title' => ''
		)
	); 
}
 
add_action( 'widgets_init', 'true_register_wp_sidebars' ); */


add_filter ('eat_exclude_types', 'my_excluded_types', 10, 1);
function my_excluded_types ( $exclude_types ){
    $exclude_types[] = 'page'; 
    return $exclude_types;
}




function create_post_type3() { // создаем новый тип записи
register_post_type( 'list_email', // указываем названия типа
array(
'labels' => array(
'name' => __( 'Письма и заявки' ), // даем названия разделу для панели управления
'singular_name' => __( 'Письмо' ), // даем названия одной записи
'add_new' => __('Добавить новое'),// далее полная русификация админ. панели
'add_new_item' => __('Добавить новое письмо'),
'edit_item' => __('Редактировать письмо'),
'new_item' => __('Новое письмо'),
'all_items' => __('Все Письма'),
'view_item' => __('Просмотр Письма'),
'search_items' => __('Поиск Письма'),
'not_found' => __('Нет Писем'),
'not_found_in_trash' => __('Письма не найдены'),
'menu_name' => 'Письма и заявки'
),
'public'              => true,
		'publicly_queryable'  => true,
'menu_position' => 7, // указываем место в левой баковой панели
'rewrite' => array('pages'=>true), // указываем slug для ссылок например: mysite/reviews/
'hierarchical' => true,
'supports' => array('title', 'editor') // тут мы активируем поддержку миниатюр
)
);
}
add_action( 'init', 'create_post_type3' ); // инициируем добавления типа

function create_post_type() { // создаем новый тип записи
register_post_type( 'news', // указываем названия типа
array(
'labels' => array(
'name' => __( 'Новости' ), // даем названия разделу для панели управления
'singular_name' => __( 'Новость' ), // даем названия одной записи
'add_new' => __('Добавить новость'),// далее полная русификация админ. панели
'add_new_item' => __('Добавить новость'),
'edit_item' => __('Редактировать новость'),
'new_item' => __('Новый новость'),
'all_items' => __('Все новости'),
'parent_item'       => 'Родит. раздел вопроса',
'parent_item_colon' => 'Родит. раздел вопроса:',
'view_item' => __('Просмотр новости'),
'search_items' => __('Поиск новостей'),
'not_found' => __('Нет новостей'),
'not_found_in_trash' => __('Новости не найдены'),
'menu_name' => 'Новости'
),
'public'              => true,
		'publicly_queryable'  => true,
'menu_position' => 5, // указываем место в левой баковой панели
'rewrite' => array('slug' => 'news', 'pages'=>true), // указываем slug для ссылок например: mysite/reviews/
'has_archive' => true,
'hierarchical' => true,
'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes') // тут мы активируем поддержку миниатюр
)
);
}
add_action( 'init', 'create_post_type' ); // инициируем добавления типа



add_filter('pre_site_transient_update_core',create_function('$a', "return null;"));
wp_clear_scheduled_hook('wp_version_check');



remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
wp_clear_scheduled_hook( 'wp_update_plugins' );


remove_action('load-update-core.php','wp_update_themes');
add_filter('pre_site_transient_update_themes',create_function('$a', "return null;"));
wp_clear_scheduled_hook('wp_update_themes');


function my_func_remove_menu(){
	remove_submenu_page( 'index.php', 'update-core.php' );
}
add_action( 'admin_menu', 'my_func_remove_menu' );

// запрет на обновление определенных плагинов
 
function filter_plugin_updates( $update ) {    
    global $DISABLE_UPDATE; // указывается в wp-config.php
    if( !is_array($DISABLE_UPDATE) || count($DISABLE_UPDATE) == 0 ){  return $update;  }
    foreach( $update->response as $name => $val ){
        foreach( $DISABLE_UPDATE as $plugin ){
            if( stripos($name,$plugin) !== false ){
                unset( $update->response[ $name ] );
            }
        }
    }
    return $update;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

function remove_menus(){
  remove_menu_page( 'edit.php?post_type=page' );
  remove_menu_page( 'index.php' );                  //Консоль
  remove_menu_page( 'edit.php' );                   //Записи
  remove_menu_page( 'edit-comments.php' );          //Комментарии
  remove_menu_page( 'tools.php' );					//Инструменты
  remove_menu_page( 'upload.php' );
  // remove_menu_page( 'themes.php' );
  // remove_menu_page( 'plugins.php' );
  remove_menu_page( 'users.php' );
  // remove_menu_page( 'options-general.php' );
  // remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'remove_menus' );




// Галлерея
add_filter('post_gallery', 'my_gallery_output', 10, 2);
function my_gallery_output( $output, $attr ){
	$ids_arr = explode(',', $attr['ids']);
	$ids_arr = array_map('trim', $ids_arr );

	$pictures = get_posts( array(
		'posts_per_page' => -1,
		'post__in'       => $ids_arr,
		'post_type'      => 'attachment',
		'orderby'        => 'post__in',
	) );

	if( ! $pictures ) return 'Запрос вернул пустой результат.';

	// Вывод
	//$out = '<div class="acritle--gallery gallery">';
	$out = '<div class="blog--list">';

	// Выводим каждую картинку из галереи <a href="img/select-house.jpg"> <div style="background-image: url(img/select-house.jpg);" class="image"></div></a>
	foreach( $pictures as $pic ){
		$src = $pic->guid;
		$t = esc_attr( $pic->post_title );
		$title = ( $t && false === strpos($src, $t)  ) ? $t : '';

		$caption = ( $pic->post_excerpt != '' ? $pic->post_excerpt : $title );

		//$out .= '<a href="'. $src .'"> <div style="background-image: url('. $src .');" class="image"></div></a>';
		$out .= '<a href="'. $src .'" class="news--item gallery">
					<img  title="'. $pic->post_excerpt .'" style="display: none;"/>
					<div class="body">
						<div style="background-image: url('. $src .');" class="image-wrap"></div>
					</div>
				</a>';
	}

	$out .= '</div>';

	return $out;
}

// Пагинация
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
	/*
	Вид базового шаблона:
	<nav class="navigation %1$s" role="navigation">
		<h2 class="screen-reader-text">%2$s</h2>
		<div class="nav-links">%3$s</div>
	</nav>

	<div class="pagination-wrap"><a href="#" class="back"></a>
        <ul>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li class="current"><a href="#">3</a></li>
            <li><a href="#">4</a></li>
        </ul><a href="#" class="next"></a>
    </div>
	*/

	return '
	<div class="pagination-wrap">
        %3$s
    </div>    
	';
}




if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Настройки сайта',
		'menu_title'	=> 'Настройки сайта',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

function do_excerpt($string, $word_limit) {
  $words = explode(' ', $string, ($word_limit + 1));
  if (count($words) > $word_limit)
  array_pop($words);
  echo implode(' ', $words).'';
}

/*function my_wp_unset_scripts() {
   wp_dequeue_script( 'jquery' );
}
add_action( 'wp_print_scripts', 'my_wp_unset_scripts', 100 );*/

/*add_filter('wp_default_scripts', 'dequeue_jquery_migrate');

function dequeue_jquery_migrate( & $scripts) {
  if (!(is_admin_bar_showing())) {
    $scripts->remove('jquery');
  }
}*/


if( !is_admin()){
   wp_deregister_script('jquery');
   wp_register_script('jquery', ("https://code.jquery.com/jquery-2.2.4.js"), false, '2.2.4');
   wp_enqueue_script('jquery');
}


function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
 
    return array(
    	'admin.php?page=theme-general-settings',
        'edit.php?post_type=list_email', // Первый разделитель
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Применить custom_menu_order
add_filter('menu_order', 'custom_menu_order');



function function_page1(){
    add_menu_page( 'custom menu title', 'Главная страница (о компании)', 'manage_options', 'post.php?post=4&action=edit', '', 'dashicons-welcome-write-blog', 0  );
}     
add_action('admin_menu','function_page1');

function function_page2(){
    add_menu_page( 'custom menu title', 'Закупочные  цены', 'manage_options', 'post.php?post=6&action=edit', '', 'dashicons-welcome-write-blog', 1  );
}     
add_action('admin_menu','function_page2');

function function_page3(){
    add_menu_page( 'custom menu title', 'Наша деятельность', 'manage_options', 'post.php?post=8&action=edit', '', 'dashicons-welcome-write-blog', 2  );
}     
add_action('admin_menu','function_page3');

function function_page4(){
    add_menu_page( 'custom menu title', 'Вакансии', 'manage_options', 'post.php?post=10&action=edit', '', 'dashicons-welcome-write-blog', 3  );
}     
add_action('admin_menu','function_page4');

function function_page5(){
    add_menu_page( 'custom menu title', 'Контакты', 'manage_options', 'post.php?post=12&action=edit', '', 'dashicons-welcome-write-blog', 4  );
}     
add_action('admin_menu','function_page5');

?>