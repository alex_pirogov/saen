// Smooth scroll
SmoothScroll({
  // Scrolling Core
 animationTime    : 800, // [ms]
 stepSize         : 80, // [px]
 // Acceleration
 accelerationDelta : 50,  // 50
 accelerationMax   : 3,   // 3
 // Keyboard Settings
 keyboardSupport   : true,  // option
 arrowScroll       : 50,    // [px]

 pulseAmdorithm   : true,
 pulseScale       : 4,
 pulseNormalize   : 1,
 // Other
 touchpadSupport   : false, // ignore touchpad by default
 fixedBackground   : true,
})
// Main slider
$(document).ready(function(){
  $('.main-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items: 1,
    animateIn: 'fadeInUp',
    animateOut: 'fadeOutUp',
    loop: true,
    nav: false,
    dots: true,
    margin: 0
  });
  $('.special-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    dots: true,
    margin: 30,
    navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
    responsiveClass: true,
    responsive:{
      0:{
        items: 1,
        nav: false
      },
      768:{
        items: 2,
        stagePadding: 20,
        nav: true
      },
    }
  });
  $('.reviews-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    dots: true,
    nav: false,
    margin: 30,
    responsiveClass: true,
    responsive:{
      0:{
        items: 1
      },
      768:{
        items: 2
      },
      992:{
        items: 3
      },
    }
  });
});
